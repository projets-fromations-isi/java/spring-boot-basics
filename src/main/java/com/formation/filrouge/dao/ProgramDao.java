package com.formation.filrouge.dao;

import com.formation.filrouge.models.ProgramModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProgramDao extends JpaRepository<ProgramModel, Long> {

  List<ProgramModel> findByDomainNames(String domain);

  Optional<ProgramModel> findByCoachId(Long coachId);
}
