package com.formation.filrouge.dao;

import com.formation.filrouge.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends JpaRepository<UserModel, Long> {
  boolean existsByEmail(String email);

  Optional<UserModel> findByEmail(String email);

  Optional<UserModel> findByEmailIgnoreCase(String username);
}
