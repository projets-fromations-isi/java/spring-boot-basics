package com.formation.filrouge.dto;

import com.formation.filrouge.models.UserModel;

import java.time.LocalDate;
import java.util.Set;

public class ProgramResponseDto {

  private Long id;

  private String name;

  private Set<String> domainNames;

  private String description;

  private LocalDate startDate;

  private LocalDate endDate;

  private UserModel coach;

  private Set<UserModel> attendees;

  public ProgramResponseDto() {}

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<String> getDomainNames() {
    return domainNames;
  }

  public void setDomainNames(Set<String> domainNames) {
    this.domainNames = domainNames;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public UserModel getCoach() {
    return coach;
  }

  public void setCoach(UserModel coach) {
    this.coach = coach;
  }

  public Set<UserModel> getAttendees() {
    return attendees;
  }

  public void setAttendees(Set<UserModel> attendees) {
    this.attendees = attendees;
  }
}
