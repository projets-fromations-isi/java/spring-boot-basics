package com.formation.filrouge.dto;

import javax.validation.constraints.NotBlank;

public class LoginRequestDto {

  @NotBlank(message = "{user.username.required}")
  private String username;

  @NotBlank(message = "{user.password.required}")
  private String password;

  private String companyId;

  public LoginRequestDto() {}

  public LoginRequestDto(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return this.username.replaceAll("\\s+","").toLowerCase();
  }

  public String getPassword() {
    return this.password;
  }

  public String getCompanyId() {
    return this.companyId;
  }
}
