package com.formation.filrouge.dto;

public class JwtResponseDto {

  private final String token;

  private final String refreshToken;

  public JwtResponseDto(String accessToken, String refreshToken) {
    this.token = accessToken;
    this.refreshToken = refreshToken;
  }

  public String getAccessToken() {
    return token;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  public String getTokenType() {
    return "Bearer";
  }
}
