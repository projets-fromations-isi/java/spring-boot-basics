package com.formation.filrouge.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserRegisterRequestDto {

  @NotBlank(message = "firstName required")
  private String firstName;

  @NotBlank(message = "lastName required")
  private String lastName;

  @Email(regexp=".*@.*\\..*", message = "{user.email.invalid}")
  private String email;

  @Pattern(regexp = "^(\\+\\d{1,3}( )?)?(\\d{2})[- .]?\\d{3}[- .]?\\d{2}[- .]?\\d{2}$", message = "{user.phoneNumber.invalid}")
  @NotBlank(message = "phoneNumber required")
  private String phoneNumber;

  @NotBlank(message = "role required")
  private String role;

  @Size(min = 5, max = 50, message = "password invalid}")
  @NotBlank(message = "password required")
  private String password;

  @Pattern(regexp = "^[mM]|[fF]$", message = "gender invalid")
  private String gender;

  @Size(min = 3, max = 100, message = "address invalid")
  private String address;

  public UserRegisterRequestDto() {}

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }
}
