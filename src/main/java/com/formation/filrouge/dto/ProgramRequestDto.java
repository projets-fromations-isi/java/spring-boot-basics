package com.formation.filrouge.dto;

import javax.validation.constraints.NotBlank;
import java.util.Set;

public class ProgramRequestDto {

  @NotBlank(message = "name is required")
  private String name;

  @NotBlank(message = "domains is required")
  private Set<String> domainNames;

  private String description;

  @NotBlank(message = "startDate is required")
  private String startDate;

  @NotBlank(message = "endDate is required")
  private String endDate;

  private Long coachId;

  public ProgramRequestDto() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<String> getDomainNames() {
    return domainNames;
  }

  public void setDomainNames(Set<String> domainNames) {
    this.domainNames = domainNames;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public Long getCoachId() {
    return coachId;
  }

  public void setCoachId(Long coachId) {
    this.coachId = coachId;
  }
}
