package com.formation.filrouge.models;

import com.formation.filrouge.dto.ProgramRequestDto;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "programs")
public class ProgramModel {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String name;

  @ElementCollection
  private Set<String> domainNames;

  private String description;

  private LocalDate startDate;

  private LocalDate endDate;

  @ManyToOne
  private UserModel coach;

  @ManyToMany
  @JoinTable(
      name = "program_attendees",
      joinColumns = @JoinColumn(name = "program_id"),
      inverseJoinColumns = @JoinColumn(name = "attendee_id"))
  private Set<UserModel> attendees = new HashSet<>();

  public ProgramModel() {}

  public void handleData(ProgramRequestDto programRequest) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
    this.name = programRequest.getName();
    this.domainNames = programRequest.getDomainNames();
    this.description = programRequest.getDescription();
    this.startDate = LocalDate.parse(programRequest.getStartDate(), formatter);
    this.endDate = LocalDate.parse(programRequest.getEndDate(), formatter);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Set<String> getDomainNames() {
    return domainNames;
  }

  public void setDomainNames(Set<String> domainNames) {
    this.domainNames = domainNames;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public UserModel getCoach() {
    return coach;
  }

  public void setCoach(UserModel coach) {
    this.coach = coach;
  }

  public Set<UserModel> getAttendees() {
    return attendees;
  }

  public void setAttendees(Set<UserModel> attendees) {
    this.attendees = attendees;
  }
}
