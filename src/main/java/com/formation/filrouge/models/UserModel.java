package com.formation.filrouge.models;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.formation.filrouge.dto.UserRegisterRequestDto;
import io.jsonwebtoken.Claims;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class UserModel {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String firstName;

  private String lastName;

  private String phoneNumber;

  private String email;

  private String address;

  private String gender;

  private String role;

  private String password;

  private boolean blocked;

  public UserModel() {}

  public UserModel(Claims claims, ObjectMapper objectMapper) {
    this.id = claims.get("id", Long.class);
    this.firstName = claims.get("firstName", String.class);
    this.lastName = claims.get("lastName", String.class);
    this.email = claims.get("email", String.class);
    this.phoneNumber = claims.get("phoneNumber", String.class);
    this.role = claims.get("role", String.class);
  }

  public void handleUserData(UserRegisterRequestDto userData) {
    this.firstName = userData.getFirstName();
    this.lastName = userData.getLastName();
    this.email = userData.getEmail();
    this.role = userData.getRole();
    this.phoneNumber = userData.getPhoneNumber();
    this.gender =  userData.getGender();
    this.address = userData.getAddress();
    this.blocked = false;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFullName() {
    return firstName + " " + lastName;
  }
  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getUsername() {
    return email;
  }
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isBlocked() {
    return blocked;
  }

  public void setBlocked(boolean blocked) {
    this.blocked = blocked;
  }
}
