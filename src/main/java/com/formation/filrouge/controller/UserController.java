package com.formation.filrouge.controller;


import com.formation.filrouge.dao.UserDao;
import com.formation.filrouge.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = {"/users"})
public class UserController {

  @Autowired
  private UserDao userDao;

  @GetMapping(path = "/list")
  public ResponseEntity<List<UserModel>> list() {
    List<UserModel> users = userDao.findAll();
    return ResponseEntity.status(HttpStatus.OK).body(users);
  }
}
