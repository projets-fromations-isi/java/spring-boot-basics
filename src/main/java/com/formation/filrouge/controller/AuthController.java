package com.formation.filrouge.controller;

import com.formation.filrouge.dao.UserDao;
import com.formation.filrouge.dto.JwtResponseDto;
import com.formation.filrouge.dto.LoginRequestDto;
import com.formation.filrouge.dto.UserRegisterRequestDto;
import com.formation.filrouge.dto.UserResponseDto;
import com.formation.filrouge.models.UserModel;
import com.formation.filrouge.security.jwt.JwtConfig;
import com.formation.filrouge.security.jwt.JwtUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.Instant;
import java.util.UUID;

@RequestMapping(value = {"/auth"})
@RestController
public class AuthController {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtUtils jwtUtils;

  @Autowired
  private JwtConfig jwtConfig;

  @Autowired
  private UserDao userDao;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @PostMapping(value = {"/login"})
  public ResponseEntity<JwtResponseDto> login(HttpServletRequest request, @Valid @RequestBody LoginRequestDto loginRequest) {
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

    UserModel user = userDao.findByEmailIgnoreCase(loginRequest.getUsername()).orElse(null);
    assert null != user;

    SecurityContextHolder.getContext().setAuthentication(authentication);

    Instant expiryDate = Instant.now().plusMillis(jwtConfig.getRefreshTokenDuration());

    String accessToken = jwtUtils.generateJwtToken(user, expiryDate.toEpochMilli());

    String refreshToken = UUID.randomUUID().toString();

    JwtResponseDto jwtData = new JwtResponseDto(accessToken, refreshToken);

    return ResponseEntity.status(HttpStatus.OK).body(jwtData);
  }

  @PostMapping(path = "/register")
  public ResponseEntity<UserResponseDto> create(@RequestBody UserRegisterRequestDto userCreateRequest) {
    if (userDao.existsByEmail(userCreateRequest.getEmail())) {
      throw new IllegalArgumentException("Cette utilisateur existe déjà");
    }

    UserModel user = new UserModel();
    user.handleUserData(userCreateRequest);
    user.setPassword(passwordEncoder.encode(userCreateRequest.getPassword()));
    userDao.save(user);

    UserResponseDto userResponseDto = new UserResponseDto();
    BeanUtils.copyProperties(user, userResponseDto);
    return ResponseEntity.status(HttpStatus.CREATED).body(userResponseDto);
  }
}
