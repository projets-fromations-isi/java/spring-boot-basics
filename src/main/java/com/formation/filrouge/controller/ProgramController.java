package com.formation.filrouge.controller;


import com.formation.filrouge.dao.ProgramDao;
import com.formation.filrouge.dao.UserDao;
import com.formation.filrouge.dto.ProgramRequestDto;
import com.formation.filrouge.dto.ProgramResponseDto;
import com.formation.filrouge.models.ProgramModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = {"/programs"})
public class ProgramController {

  @Autowired
  private ProgramDao programDao;

  @Autowired
  private UserDao userDao;

  @GetMapping(path = "/list")
  public ResponseEntity<List<ProgramModel>> list() {
    List<ProgramModel> programs = programDao.findAll();
    return ResponseEntity.status(HttpStatus.OK).body(programs);
  }

  @PostMapping(path = "/create")
  public ResponseEntity<ProgramResponseDto> create(@RequestBody ProgramRequestDto programRequest) {
    ProgramModel program = new ProgramModel();
    program.handleData(programRequest);
    if (programRequest.getCoachId() != null && userDao.findById(programRequest.getCoachId()).isPresent()) {
      program.setCoach(userDao.findById(programRequest.getCoachId()).get());
    }
    programDao.save(program);

    ProgramResponseDto programResponseDto = new ProgramResponseDto();
    BeanUtils.copyProperties(program, programResponseDto);
    return ResponseEntity.status(HttpStatus.CREATED).body(programResponseDto);
  }

  @PutMapping(path = "/update/{id}")
  public ResponseEntity<ProgramResponseDto> update(@PathVariable(name = "id", required = true) Long id,
                                                   @RequestBody ProgramRequestDto programRequest) {
    ProgramModel program = programDao.findById(id).orElse(null);
    assert null != program;
    program.handleData(programRequest);
    if (programRequest.getCoachId() != null && userDao.findById(programRequest.getCoachId()).isPresent()) {
      program.setCoach(userDao.findById(programRequest.getCoachId()).get());
    }
    programDao.save(program);

    ProgramResponseDto programResponseDto = new ProgramResponseDto();
    BeanUtils.copyProperties(program, programResponseDto);
    return ResponseEntity.status(HttpStatus.OK).body(programResponseDto);
  }

  @DeleteMapping(path = "/delete/{id}")
  public ResponseEntity<Map<String, String>> delete(@PathVariable(name = "id", required = true) Long id) {
    ProgramModel program = programDao.findById(id).orElse(null);
    assert null != program;
    programDao.delete(program);
    ProgramResponseDto programResponseDto = new ProgramResponseDto();
    BeanUtils.copyProperties(program, programResponseDto);
    return ResponseEntity.status(HttpStatus.OK).body(Map.of("message", "deleted"));
  }
}
