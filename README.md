Une fois cloner le projet:

## Installer java ^11

## Installer mysql

## Installer maven

## Créer la base de données `filRougeDB`:
>`CREATE DATABASE filRougeDB CHARACTER SET utf8 COLLATE utf8_general_ci;`

## Entrer dans le dossier du projet

## Lancer la commande:
>`mvn clean install`

## Lancer l'application via l'IDE ou par ligne de commande:
>`mvn spring boot:run`
## Liste des api:
### Inscription:
API: `http://127.0.0.1:8080/api/v1/auth/login`
DATA:
```
    {
        "firstName": "Ousmane",
        "lastName": "NDIAYE",
        "phoneNumber": "775919686",
        "email": "ousmanendiaye352@gmail.com",
        "address": "Colobane, Dakar",
        "password": "passer123"
    }
```
